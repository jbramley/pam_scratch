pam_scratch.so: pam_scratch.o
	ld -x -shared -o pam_scratch.so pam_scratch.o

pam_scratch.o: pam_scratch.c
	gcc -fPIC -DLINUX_PAM -Dlinux -Di386 -DPAM_DYNAMIC  -c pam_scratch.c

install: pam_scratch.so
	install --mode=744 pam_scratch.so /lib/security

clean:
	rm -f pam_scratch.o pam_scratch.so
