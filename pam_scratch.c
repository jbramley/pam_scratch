#include <stdio.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_ext.h>

PAM_EXTERN int pam_sm_open_session (pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	int retval;
	const char* username;
	const char* base = "/scratch/";
	const char* ssd  = "/ssd";
	struct stat st;
	const struct passwd *pwd;

	retval = pam_get_user(pamh,&username,"Username : ");
	
	char* dir = malloc(strlen(base) + strlen(username) + 2);
	sprintf(dir,"%s%s",base,username);
	mkdir(dir, S_IRWXU);
	
	pwd = getpwnam(username);

	if (pwd == NULL) {
		return PAM_CRED_INSUFFICIENT;
	}



	chown(dir,pwd->pw_uid,pwd->pw_gid);
	free(dir);

	if(stat(ssd,&st)==0) {
		dir = malloc(strln(ssd)+strlen(username) + 2);
		sprintf(dir,"%s%s",ssd,username);
		mkdir(dir,S_IRWXU);
		chown(dir,pwd->pw_uid,pwd->pw_gid);
		free(dir);		
	}

	return PAM_SUCCESS;


}



/* Ignore */
PAM_EXTERN int pam_sm_close_session (pam_handle_t *pamh,  int flags, int argc, const char **argv)
{
	   return PAM_SUCCESS;
}
